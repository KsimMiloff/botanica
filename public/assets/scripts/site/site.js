ymaps.ready(init);

function init() {

    function findClosestObjects (map, objectManager, user_location) {

        closest = ymaps
            .geoQuery(objectManager.objects)
            .getClosestTo(user_location);

        map.setBounds([
            user_location.geometry.getCoordinates(),
            closest.geometry.getCoordinates()
        ]);

    }

    var previewMap = new ymaps.Map('preview-map', {
            center: [43.2393,76.9349],
            zoom: 12,
            controls: ["zoomControl", "fullscreenControl"]
        }, {
            searchControlProvider: 'yandex#search'
        }),

        objectManager = new ymaps.ObjectManager({
            clusterize: true, // Чтобы метки начали кластеризоваться, выставляем опцию.
            gridSize: 32 // ObjectManager принимает те же опции, что и кластеризатор.
        });


    $.ajax({
        url: "/contacts/coords"
    }).done(function(data) {
        objectManager.add(data);
    });

    // Чтобы задать опции одиночным объектам и кластерам,
    // обратимся к дочерним коллекциям ObjectManager.
    objectManager.objects.options.set('preset', 'islands#greenDotIcon');
    objectManager.clusters.options.set('preset', 'islands#greenClusterIcons');

    previewMap.geoObjects.add(objectManager);


    // Добавим элемент управления с собственной меткой геолокации на карте.
    var geolocationControl = new ymaps.control.GeolocationControl({
        data: {content: 'Рядом со мной'},
        options: {maxWidth: 300}
    });
    geolocationControl.events.add('click', function (event) {
        var geolocation = ymaps.geolocation;

        geolocation.get({
            provider: 'browser',
            autoReverseGeocode: false,
            mapStateAutoApply: false
        })

            .then( function (result) {
                user_location = result.geoObjects.get(0);
                previewMap.geoObjects.add(result.geoObjects);

                closest = ymaps.geoQuery(ymaps.geocode( user_location.geometry.getCoordinates() ))
                    .then( // Нужно дождаться ответа от сервера и только потом обрабатывать полученные результаты.
                        findClosestObjects( previewMap, objectManager, user_location )
                    );
            });

    });


    previewMap.controls.add(geolocationControl);

}


$(function() {
    $(document).on('click', '.comps-title', function () {
        $('.comps-title').not(this).removeClass('open');
        $(this).toggleClass('open');
    });
});
