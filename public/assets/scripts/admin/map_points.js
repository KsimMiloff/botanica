ymaps.ready(init);

function init() {
    if ( $( '#touchable-map').length ) {
        initPointable();
    }

    if ( $( '#preview-map').length ) {
        initPreview();
    }
}


function initPreview() {
    var previewMap = new ymaps.Map('preview-map', {
            center: [43.2393,76.9349],
            zoom: 12,
            controls: ["zoomControl", "fullscreenControl"]
        }, {
            searchControlProvider: 'yandex#search'
        }),
        objectManager = new ymaps.ObjectManager({
            // Чтобы метки начали кластеризоваться, выставляем опцию.
            clusterize: true,
            // ObjectManager принимает те же опции, что и кластеризатор.
            gridSize: 32
        });

    // Чтобы задать опции одиночным объектам и кластерам,
    // обратимся к дочерним коллекциям ObjectManager.
    objectManager.objects.options.set('preset', 'islands#greenDotIcon');
    objectManager.clusters.options.set('preset', 'islands#greenClusterIcons');
    previewMap.geoObjects.add(objectManager);

    $.ajax({
        url: "/admin/mpoints/coords"
    }).done(function(data) {
        objectManager.add(data);
    });


    $(document).on('click', '[data-behavior=deleteMpoint]', function(e) {
        e.preventDefault();

        csrftoken = $('meta[name=_token]').attr('content');

        $.ajax({
            url: $(this).data('url'),
            type: 'post',
            data: {_method: 'delete'},
            dataType: "json",

            beforeSend: function(request)
            {
                return request.setRequestHeader('X-CSRF-Token', csrftoken);
            },

            success: function (response) {
                objectManager.remove([response['id']]);
            },

            error: function() {
                alert('Удалить точку не удалось');
            }
        })
    })

}


function initPointable() {
    var point,
        pointableMap = new ymaps.Map('touchable-map', {
            center: [43.2393,76.9349],
            zoom: 12,
            controls: ["zoomControl", "fullscreenControl"]
        }, {
            searchControlProvider: 'yandex#search'
        });

    $lat = $("input[name='mpoint[lat]']");
    $lon = $("input[name='mpoint[lon]']");


    var delay = (function(){
        var timer = 0;
        return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
        };
    })();


    $("[name='mpoint[address]']").on('keyup', function() {

        text = $(this).val();

        delay( function() {

            ymaps.geocode('Алматы ' + text , {
                results: 1
            })
                .then(function (res) {
                    // Выбираем первый результат геокодирования.
                    var firstGeoObject = res.geoObjects.get(0),
                        coords = firstGeoObject.geometry.getCoordinates(), // Координаты геообъекта.
                        bounds = firstGeoObject.properties.get('boundedBy'); // Область видимости геообъекта.

                    // Добавляем первый найденный геообъект на карту.
                    createOrMovePlacemark(coords);

                    // Масштабируем карту на область видимости геообъекта.
                    pointableMap.setBounds(bounds, {
                        checkZoomRange: true // Проверяем наличие тайлов на данном масштабе.
                    });

                });

        }, 1000 );

    });

    // если у нас уже есть координаты
    pointFromInputCoords(pointableMap);

    // Слушаем клик на карте
    pointableMap.events.add('click', function (e) {
        var coords = e.get('coords');

        createOrMovePlacemark(coords)
        getAddress(coords);
    });


    function createOrMovePlacemark(coords) {
        // Если метка уже создана – просто передвигаем ее
        if (point) {
            point.geometry.setCoordinates(coords);
        }
        // Если нет – создаем.
        else {
            point = createPlacemark(coords);
            pointableMap.geoObjects.add(point);
        }
    }

    // Создание метки
    function createPlacemark(coords) {
        point = new ymaps.Placemark(coords, {
            //iconContent: 'поиск...'
        }, {
            preset: 'islands#violetStretchyIcon',
            draggable: true
        });

        // Слушаем событие окончания перетаскивания на метке.
        point.events.add('dragend', function () {
            getAddress(point.geometry.getCoordinates());
        });

        return point;
    }


    // Определяем адрес по координатам (обратное геокодирование)
    function getAddress(coords) {
        //point.properties.set('iconContent', 'поиск...');
        ymaps.geocode(coords).then(function (res) {
            var firstGeoObject = res.geoObjects.get(0);


            $("[name='mpoint[title]']").val(firstGeoObject.properties.get('name'));
            $("[name='mpoint[address]']").val(firstGeoObject.properties.get('text'));

            //point.properties
            //    .set({
            //        //iconContent: firstGeoObject.properties.get('name'),
            //        //balloonContent: firstGeoObject.properties.get('text')
            //    });
        });

        setCoordsToInput(coords);
    }


    function setCoordsToInput(coords) {
        $lat.val(coords[0]);
        $lon.val(coords[1]);
    }

    function pointFromInputCoords(map) {
        if ($lat.val() && $lon.val()) {
            latlon = [$lat.val(), $lon.val()];
            point = createPlacemark(latlon);
            // сразу добавляем точку на карте
            map.geoObjects.add(point);
        }
    }

}