<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Webpatser\Uuid\Uuid;


class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->nullable();
            $table->string('title');
            $table->string('category_id');
            $table->string('state');
            $table->text('desc')->nullable();
            $table->string('image_id')->nullable();
            $table->text('props')->nullable();
            $table->boolean('is_visible')->nullable();

            $table->string('locale_id');
            $table->string('localed_key');

            $table->timestamps();

        });


        $arr = [
            [
                'title'  => 'Главная',
                'category_id' => 'slider',
                'props' => json_encode([]),
            ],
            [
                'title'  => 'О компании',
                'category_id' => 'page',
                'props' => json_encode([]),
            ],
            [
                'title'  => 'О Заводе',
                'category_id' => 'page',
                'props' => json_encode([]),
            ],
            [
                'title'  => 'Nomad',
                'category_id' => 'tea',
                'props' => json_encode(['css_class' => 'nomad']),
            ],
            [
                'title'  => 'Hammam',
                'category_id' => 'tea',
                'props' => json_encode(['css_class' => 'hammam']),
            ],
            [
                'title'  => 'Yoga',
                'category_id' => 'tea',
                'props' => json_encode(['css_class' => 'yoga']),
            ]
        ];

        foreach($arr as &$item) {
            $item['locale_id'] = 'ru';
            $item['localed_key'] = Uuid::generate();
            $item['state'] = 'live';
            $item['is_visible'] = true;
        }


        DB::table('contents')->insert($arr);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contents');
    }
}
