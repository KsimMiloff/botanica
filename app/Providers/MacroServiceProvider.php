<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class MacroServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        require base_path() . '/resources/macros/form/forms.php';
        require base_path() . '/resources/macros/form/files.php';
        require base_path() . '/resources/macros/form/fields.php';
        require base_path() . '/resources/macros/html/pictures.php';
        // etc...
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}