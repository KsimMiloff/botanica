<?php

namespace App;

use App\Models\Concerns\LifeCycle;
use App\Models\ExtendedFields\Category\HasXFCategoryMixin;
use App\Models\Concerns\SeoableMixin;
use App\Models\Content\Category;
use Webpatser\Uuid\Uuid;

class Content extends BaseXFModel
{
    const CATEGORY_CLASS = Category::class;

    use LifeCycle {
        LifeCycle::__construct as private __LifeCycleConstruct;
    }

    use HasXFCategoryMixin, SeoableMixin;

    protected $table = 'contents';

    protected $guarded = [];

    protected $casts = [
        'image_id'   => 'integer',
        'props'      => 'array',
    ];


    protected $attributes = [
        'is_visible' => true,
    ];


    protected $rules = array(
        'title'       => 'required',
        'locale_id'   => 'required',
        'category_id' => 'required|in:page,article,slider,tea',
//        'desc'        => 'required',
    );


    public function __construct($attributes = [])
    {
        $this->__LifeCycleConstruct();

        // устанавливаем значение по умолчанию, отработает только для нового объекта
        $this->attributes = array_add($this->attributes, 'localed_key', Uuid::generate());
//        $this->attributes['localed_key'] = 'live';

        parent::__construct($attributes);
    }


    public function locale()
    {
        return $this->belongsTo('App\Locale', 'locale_id');
    }


    public function to_locale($locale_id)
    {
        if ($this->locale_id == $locale_id) {
            return $this;
        }

        $localed = static::where([
            ['locale_id', $locale_id],
            ['localed_key', $this->localed_key],
        ])->first();


        return $localed;
    }


    public function scopeVisible($query) {
        return $query->where('is_visible', true);
    }


    public function scopeLocaled($query) {
        return $query->where('locale_id', \App::getLocale());
    }


    public function scopeForMainPage($query) {
        return $query->where('is_visible', true);
    }


    public function getSeotitleAttribute()
    {
        return $this->title;
    }


    public function xfields()
    {
        return $this->category->fields();
    }


    public function slugable() {
        return $this->title; // поле из которого строиться слаг
    }

}
