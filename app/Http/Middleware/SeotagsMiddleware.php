<?php

namespace App\Http\Middleware;

use Closure;
use App\Seotag;

class SeotagsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $seotags = new Seotag([
            'title' => 'матрасы алматы, ортопедические матрасы алматы, купить матрас алматы, ортопедические матрасы купить, купить матрас ортопедический недорого, магазин матрасов',
            'keywords' => 'матрасы алматы, ортопедические матрасы алматы, купить матрас алматы, ортопедические матрасы купить, купить матрас ортопедический недорого, магазин матрасов',
            'descriptions' => 'Хороший и здоровый сон начинается с нашего интернет-магазина. Интернет-магазин мир матрасов - это качественные ортопедические матрасы, комфортабельные кровати и постельные принадлежности оптом'
        ]);
        view()->share('seotags', $seotags);
        return $next($request);
    }
}
