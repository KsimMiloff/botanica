<?php

namespace App\Http\Middleware;

use Closure;
use App\Content;

class TeaListMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale   = $request->get('locale', \App::getLocale());
        $tea_list = Content::visible()->byCategoryId('tea')->where('locale_id', 'ru')->get();

        view()->share('locale', $locale);
        view()->share('tea_list', $tea_list);
        view()->share('site_menu', $this->site_menu($locale));

        return $next($request);
    }

    private function site_menu($locale = 'ru') {
        // коряво, зато просто: сначала получаем русский контент, т.к. он точно есть, а потом ищем его локализованную версию
        $contents = [];
//        $contents[] = Content::visible()->where('locale_id', 'ru')->find(2)->to_locale($locale); // о компании
        $contents[] = Content::visible()->where('locale_id', 'ru')->find(3)->to_locale($locale); // о заводе

        $teas = Content::visible()->byCategoryId('tea')->where('locale_id', 'ru')->get();
        $tea_subs = [];
        $tea_ids = [];
        foreach($teas as $tea)
        {
            $localed_tea = $tea->to_locale($locale);
            if ($localed_tea)
            {
                $tea_ids[] = $localed_tea->id;
                $tea_subs[] = [
                    'title' => $localed_tea->title,
                    'url' => locale_route('contents.picture', ['id' => $localed_tea->seo_id]),
                    'id' => [$localed_tea->id],
                ];
            }
        }

        $menu = [
             [
                 'title' => Content::visible()->where('locale_id', 'ru')->find(1)->to_locale($locale)->title, // главная
                 'url' => '/',
                 'id' => ['/']
             ],
            [
                'title' => ['ru' => 'Продукция', 'kz' => 'Өнім'][$locale],
                'subs' => $tea_subs,
                'id' => $tea_ids
            ],
            [
                'title' => ['ru' => 'О компании', 'kz' => 'Компания туралы'][$locale],
                'url' => locale_route('about'),
                'id' => ['about']
            ]
        ];

        foreach($contents as $content) {
            if ($content) {
                $menu[] = [
                    'title' => $content->title,
                    'url' => locale_route('contents.show', ['id' => $content->seo_id]),
                    'id' => [$content->id],
                ];
            }
        }

        $menu[] = [
            'title' => ['ru' => 'Контакты', 'kz' => 'Байланыс'][$locale],
            'url' => locale_route('contacts'),
            'id' => ['contacts']
        ];


        return $menu;
    }
}
