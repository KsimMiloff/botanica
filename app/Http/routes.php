<?php

Route::group(['middleware' => ['web']], function () {

    Route::get('/login', ['as' => 'auth.login', 'uses' => 'Auth\AuthController@getLogin']);
    Route::post('/login', ['as' => 'auth.login', 'uses' => 'Auth\AuthController@postLogin']);
    Route::get('/logout', ['as' => 'auth.logout', 'uses' => 'Auth\AuthController@logout']);

    Route::group(['namespace' => 'File'], function()
    {
        Route::get('images/{id}/resized/{size}/{crop?}',
            ['as' => 'images.resized', 'uses' => 'ImagesController@resize']
        );
        Route::resource('images', 'ImagesController', ['except' => ['destroy']]);
        Route::resource('files', 'FilesController', ['except' => ['destroy']]);
    });


    Route::get('admin', function () {
        return redirect('/admin/contents');
    });


    Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => 'auth'], function()
    {
        Route::get('contents/create/{category_id?}', ['as' => 'admin.contents.create', 'uses' => 'ContentsController@create']);
        Route::get('contents/localize/{contents}/{locale}', ['as' => 'admin.contents.localize', 'uses' => 'ContentsController@localize']);
        Route::resource('contents', 'ContentsController', ['except' => ['create']]);

        Route::get('mpoints/coords', ['as' => 'mpoints.coords', 'uses' => 'MapPointsController@coords']);
        Route::resource('mpoints', 'MapPointsController');

        Route::resource('users', 'UsersController', ['except' => ['show', 'destroy']]);

    });

    Route::get('contacts/coords', ['as' => 'contacts.coords','uses' => 'ContactsController@coords']);

    Route::group(['prefix' => '{locale?}','middleware' => ['locale']], function () {

        Route::group(['middleware' => ['tea_list']], function()
        {


            Route::get('makeup', ['as' => 'makeup', function () {
                return view('makeup.index');

            }]);


            Route::get('nistruction', ['as' => 'nistruction', function () {
                return view('makeup.nistruction');

            }]);


            Route::get('text', ['as' => 'text', function () {
                return view('makeup.text');

            }]);

            Route::get('gallery', ['as' => 'text', function () {
                return view('makeup.gallery');

            }]);


            Route::get('/', 'WelcomeController@index');

            Route::get('contacts', ['as' => 'contacts', 'uses' => 'ContactsController@index']);
            Route::get('about', ['as' => 'about', 'uses' => 'ContentsController@about']);

            Route::get('{id}', ['as' => 'contents.show','uses' => 'ContentsController@show']);
            Route::get('{id}/components', ['as' => 'contents.comps','uses' => 'ContentsController@comps']);
            Route::get('{id}/instruction', ['as' => 'contents.instruction','uses' => 'ContentsController@instruction']);
            Route::get('{id}/instruction', ['as' => 'contents.instruction','uses' => 'ContentsController@instruction']);
            Route::get('{id}/picture', ['as' => 'contents.picture','uses' => 'ContentsController@picture']);

        });
    });

});
