<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\MapPoint;

class ContactsController extends Controller
{
    public function index()
    {

        $mpoints = MapPoint::get();

        return view('contacts.index', [
            'mpoints' => $mpoints,
            'menu_id' => 'contacts' // для того, чтобы ставить класс 'active' в менюшке
        ]);
    }

    public function coords()
    {
        $coords = [
            "type" => "FeatureCollection",
            "features" => MapPoint::get()->map(function($value, $key) {
                return [
                    'type' => 'Feature',
                    "id" => $value->id,
                    "geometry" => ["type" => "Point", "coordinates" => $value->latlon],
                    "properties" => ["balloonContent" => $value->address, "clusterCaption" => $value->title, "hintContent" => $value->title],
                ];
            })
        ];

        return response()->json($coords);
    }

}
