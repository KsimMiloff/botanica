<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use App\MapPoint;

class MapPointsController extends Controller
{
    public function index()
    {
        $mpoints = MapPoint::get();
        return view('admin.map_points.index', [
            'mpoints' => $mpoints,
        ]);
    }

    public function coords()
    {
        $coords = [
            "type" => "FeatureCollection",
            "features" => MapPoint::get()->map(function($value, $key) {
                $balloon_content = $value->address . '<br/><a href="#" data-behavior="deleteMpoint" data-url="' . route('admin.mpoints.destroy', [$value->id]) . '">удалить</a>';

                return [
                    'type' => 'Feature',
                    "id" => $value->id,
                    "geometry" => ["type" => "Point", "coordinates" => $value->latlon],
                    "properties" => ["balloonContent" => $balloon_content, "clusterCaption" => $value->title, "hintContent" => $value->title],
                ];
            })
        ];

        return response()->json($coords);
    }


    public function create()
    {
        $mpoint = new MapPoint();

        return view('admin.map_points.new', [
            'mpoint' => $mpoint,
            'crumbs' => $this->crumbs($mpoint),
        ]);
    }

    public function store(Request $request)
    {
        $mpoint = new MapPoint($this->mpoint_params($request));

        if ( $mpoint->save() ) {
            flash()->success('Точка доавлена');
            return redirect()->route( 'admin.mpoints.index');
        }

        flash()->error('Точка не была добвлена!');
        return view('admin.map_points.new', [
            'mpoint' => $mpoint,
            'crumbs'  => $this->crumbs($mpoint)
        ]);

    }


    public function edit($id)
    {

        $mpoint = MapPoint::find($id);

        return view('admin.map_points.edit', [
            'mpoint' => $mpoint,
            'crumbs'  => $this->crumbs($mpoint)
        ]);
    }


    public function update(Request $request, $id)
    {
        $mpoint = MapPoint::find($id)
            ->fill( $this->mpoint_params($request) );

        if ( $mpoint->save() ) {
            flash()->success('Точка изменена');
            return redirect()->route( 'admin.mpoints.index');
        }

        flash()->error('Изменить тоску не удалось');
        return view('admin.map_points.edit', [
            'mpoint' => $mpoint,
            'crumbs'  => $this->crumbs($mpoint)
        ]);
    }

    public function destroy(Request $request, $id)
    {

        $point = MapPoint::find($id);
        if ( $point && $point->delete() )
        {
            return (new Response(['id' => $id], 200));
        } else {
            return (new Response(['error' => true, 'msg' => 'Bad request'], 403));
        }

    }


    private function mpoint_params($request)
    {
        return $request->mpoint ?: [];
    }


    private function crumbs($mpoint)
    {
        $crumbs = [['title' => 'Точки на карте', 'url' => route('admin.mpoints.index')]];

        if ( $mpoint->exists ) {
            $crumb = ['title' => "Редактривание точки", 'active' => true];
        } else {
            $crumb = ['title' => "Добавление точки", 'active' => true];
        }

        array_push($crumbs, $crumb);

        return collect($crumbs);
    }
}
