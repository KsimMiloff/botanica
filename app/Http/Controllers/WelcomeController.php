<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Content;


class WelcomeController extends Controller
{

    public function index()
    {
        $locale = \App::getLocale();
        $content = Content::live()->visible()->find(1)->to_locale($locale);
//        $content->id = '/'; // для того, чтобы ставить класс 'active' в менюшке

        return view('welcome.index', [
            'content' => $content,
        ]);
    }

}
