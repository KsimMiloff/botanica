<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Content;
use App\Models\Content\Category;

class ContentsController extends Controller
{

    public function __construct(Request $request)
    {

        if ($request->route('id')) {
            $this->content = Content::localed()->bySlug($request->route('id'))->first();
        }

        if ($request->route('category_id')) {
            $this->category = Category::find( $request->route( 'category_id' ) );

            if ( is_null( $this->category ) ) {
                abort(404);
            }
        }
    }

    public function show($id)
    {
        return view('contents.show', [
            'content' => $this->content,
            'menu_id' => $this->content->id
        ]);
    }

    public function about() {
        $contents = Content::where('locale_id', 'ru')->where('category_id', 'article')->get();
        return view('contents.index', [
            'contents' => $contents,
            'menu_id' => 'about'
        ]);
    }

    public function comps($id)
    {
        return view('contents.comps', [
            'content' => $this->content,
            'menu_id' => $this->content->id
        ]);
    }

    public function instruction($id)
    {
        return view('contents.instruction', [
            'content' => $this->content,
            'menu_id' => $this->content->id
        ]);
    }


    public function picture($id)
    {
        return view('contents.picture', [
            'content' => $this->content,
            'menu_id' => $this->content->id
        ]);
    }

}
