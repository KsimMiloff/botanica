<?php

namespace App\Models\Content;

use App\Models\ExtendedFields\Field\BaseField;
use App\Models\ExtendedFields\Category\BaseCategory;

class Category extends BaseCategory
{

    public static function hash() {
        return [
            'page' => [
                'singular' => ['Страница'],
                'plural'   => ['Страницы'],
            ],
            'article' => [
                'singular' => ['Статья'],
                'plural'   => ['Статьи'],
            ],
            'slider'    => [
                'singular' => ['Слайдер'],
                'plural'   => ['Слайдеры'],
            ],
            'tea'    => [
                'singular' => ['Чай'],
                'plural'   => ['Чаи'],
            ],
        ];
    }


    public static function config()
    {
        return [
            'slider' => [
                'slider_ids' => BaseField::create([
                    'title'   => 'Слайдер',
                    'storage' => 'props.image_ids',
                    'type'    => 'Album',
                    'is_json' => true,
                ]),
            ],

            'page'  => [
                'desc' => BaseField::create([
                    'title'   => 'Описание',
                    'storage' => 'desc',
                    'type'    => 'Wisiwyg',
                ]),

                'slider_ids' => BaseField::create([
                    'title'   => 'Слайдер',
                    'storage' => 'props.image_ids',
                    'type'    => 'Album',
                    'is_json' => true,
                ]),

            ],

            'article'  => [
                'desc' => BaseField::create([
                    'title'   => 'Описание',
                    'storage' => 'desc',
                    'type'    => 'Wisiwyg',
                ]),

                'image_id' => BaseField::create([
                    'title'   => 'Картинка',
                    'storage' => 'props.image_id',
                    'type'    => 'Image',
                    'is_json' => true,
                ]),

                'image_desc' => BaseField::create([
                    'title'   => 'Текст под картинкой',
                    'storage' => 'props.image_desc',
                    'type'    => 'Text',
                ]),


            ],

            'tea'  => [

                'css_class' => BaseField::create([
                    'title'   => 'CSS-класс',
                    'storage' => 'props.css_class',
                    'type'    => 'Hidden',
                ]),

                'slogan' => BaseField::create([
                    'title'   => 'Слоган',
                    'storage' => 'props.slogan',
                    'type'    => 'String',
                ]),

                'components' => BaseField::create([
                    'title'   => 'Состав',
                    'storage' => 'props.components',
                    'type'    => 'Wisiwyg',
                ]),

                'desc' => BaseField::create([
                    'title'   => 'Описание',
                    'storage' => 'desc',
                    'type'    => 'Wisiwyg',
                ]),

                'image_id' => BaseField::create([
                    'title'   => 'Иконка',
                    'storage' => 'props.image_id',
                    'type'    => 'Image',
                    'is_json' => true,
                ]),

                    'page_image_id' => BaseField::create([
                    'title'   => 'Страница-картинка',
                    'storage' => 'props.page_image_id',
                    'type'    => 'Image',
                    'is_json' => true,
                ]),
            ],
        ];
    }

}
