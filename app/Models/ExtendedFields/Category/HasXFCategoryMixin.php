<?php

namespace App\Models\ExtendedFields\Category;

trait HasXFCategoryMixin
{

//    public function __construct() {
//
//    }

    public function getCategoryAttribute()
    {
        $id = array_get($this->attributes, 'category_id', null);

        // надо повторно разобраться и откомментить :), а то все забыл
        if ( $id ) {
            return forward_static_call_array([static::CATEGORY_CLASS, 'find'], [$id]);
        } else {
            return forward_static_call_array([static::CATEGORY_CLASS, 'create'], [null, null]);
        }
    }

    // scope byCategoryId
    public function scopeByCategoryId($query, $category_id) {
        return $query->where('category_id', $category_id);
    }


}