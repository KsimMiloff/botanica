<?php

namespace App\Models\ExtendedFields\Category;

use App\Models\ExtendedFields\Field;

class BaseCategory
{

    public function __construct($id, $titles) {
        $this->id    = $id;
        $this->singular_titles = $titles['singular'];
        $this->plural_titles   = $titles['plural'];

        $this->title        = $this->singular_titles[0];
        $this->plural_title = $this->plural_titles[0];
    }


    public static function create($id, $titles) {
        return new static($id, $titles);
    }


    public function fields()
    {
        return $this->id ? static::config()[$this->id] : [];
    }


    public function siblings()
    {
        return static::all()->except($this->id);
    }


    public static function hash()
    {
        // список категорий, определить в наследнике
        return [];
    }


    public static function ids()
    {
        return collect(static::hash())->keys()->all();
    }


    public static function all() {
        // внутри колбека коллекции сбивается namespace, поэтому захватываем нужный namespace заранее
        $class = static::class;

        return collect( static::hash() )->map( function ( $titles, $id ) use ( $class )  {
            return new $class($id, $titles);
        });
    }

    public static function find($id) {

        return static::all() -> filter( function ($category) use ( $id ) {
            return $category->id == $id;
        }) -> first();
    }


    public static function config()
    {
        // конфиг уникальных полей для каждой из категорий, определить в наследнике
        return [];
    }

}
