<?php

namespace App\Models\ExtendedFields\Field;

use Form;

class BooleanField extends BaseField
{

    public function from_db_format($value)
    {
        $value = is_null($value) ? $this->value : $value;
        return $value;
    }

    public function to_db_format($value)
    {
       return !!$value;
    }

    public function field($field_alias, $value = null)
    {
        return Form::boolean($field_alias, $value);
    }

    public function widget($options, $class=null)
    {
        $class = 'col-sm-3 col-sm-offset-2';
        return parent::widget($options, $class);
    }

    public function html($label, $field, $class)
    {
        return "
            <div class='form-group'>
                <div class='{$class}'>
                    <div class='checkbox'>
                        <label>
                             $field $this->title
                         </label>
                    </div>
                </div>
            </div>
        ";
    }
}