<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seotag extends Model
{

    public $timestamps = false;

    protected $table = 'seotags';

    protected $guarded = [];

    protected $title_prefix = 'Алматинская матрасная фабрика';

    public function seoable()
    {
        return $this->morphTo();
    }

    public function getSiteTitleAttribute()
    {
        $title = $this->title ?: $this->seoable->seotitle;

        return $this->title_prefix . ' | ' .$title;
    }
}
