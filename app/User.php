<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    protected $rules = [
        'email' => 'required|email',
        'password' => 'required',
        'name'  => 'required'
    ];


    public function validate($data) {
        $this->rules['email'] .= '|unique:users,email,'.$this->id; // исключаем из проверки на уникальность email текущего пользователя
        return parent::validate($data);
    }


    public function set_password($pass) {
        $this->password = bcrypt($pass);
    }
}
