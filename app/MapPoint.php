<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MapPoint extends Model
{
    protected $table = 'map_points';
    protected $guarded = [];

    public function getLatlonAttribute() {
        return [$this->lat, $this->lon];
    }

}
