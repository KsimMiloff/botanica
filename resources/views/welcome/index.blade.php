@extends('layouts.site')

@section('content')

    <tr>
        @include('partials._logo')
        <td>

            <div class="fotorama" data-width="100%" data-autoplay="true">
                @foreach($content->slider_ids as $image_id)
                    {!! Html::picture($image_id, ['size' => '1050x522', 'crop' => true]) !!}
                @endforeach

            </div>

        </td>
    </tr>

@stop
