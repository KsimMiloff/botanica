<!DOCTYPE html>
<html lang="{!! \App::getLocale() !!}">
    <head>
        <meta charset="utf-8">

        <link href="{{ asset('/vendor/styles/reset.css') }}" rel="stylesheet">
        <link href="{{ asset('/vendor/styles/fotorama.css') }}" rel="stylesheet">

        <link href="{{ asset('/assets/styles/site/main.css') }}" rel="stylesheet">
        <link href="{{ asset('/assets/styles/site/comps.css') }}" rel="stylesheet">
        <link href="{{ asset('/assets/styles/site/wysiwyg.css') }}" rel="stylesheet">


        <script src="{{ asset('/vendor/scripts/jquery.js') }}"></script>
        <script src="{{ asset('/vendor/scripts/fotorama.js') }}"></script>

        <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
        <script src="{{ asset('/assets/scripts/site/site.js') }}"></script>

    </head>
    <body>

        <div class="header">
            <table class="body-tbl">
                <colgroup>
                    <col class="arnament-col">
                    <col width="*">
                </colgroup>
                <tr class="">
                    <td></td>
                    <td colspan="2" class="menu-cell">

                        @include('partials._menu')

                    </td>
                </tr>
            </table>
        </div>

        <table class="body-tbl">
            <colgroup>
                <col class="arnament-col">
                <col class="logo-col">
                <col width="*">
            </colgroup>

            @yield('content')

            <tr>
                <td></td>
                <td></td>
                <td class="tea-menu">

                    @foreach($tea_list as $tea_item)
                        <?php $localed_item = $tea_item->to_locale($locale); ?>

                        @if ($localed_item)

                            <div class="tea-menu-item">

                                <a class="menu-link" href="{!! locale_route('contents.picture', ['id' => $localed_item->seo_id]) !!}">
                                    {!! Html::picture($localed_item->image_id, ['size' => '133x111', 'crop' => true, 'class' => '']) !!}
                                </a>

                                <div class="text-link-container">
                                    <a href="{!! locale_route('contents.picture', ['id' => $localed_item->seo_id]) !!}">
                                        <img src="{!! URL::asset('assets/images/tea-menu-arrow.png') !!}" class="arrow2">

                                                    <span>
                                                        {!! $localed_item->title !!}
                                                        <br/>
                                                        <small>{!! $localed_item->slogan !!}</small>
                                                    </span>
                                    </a>
                                </div>

                                <br/>
                            </div>

                        @endif

                    @endforeach

                </td>
            </tr>
        </table>

    </body>
</html>

{{--<!DOCTYPE html>--}}
{{--<html lang="en">--}}
    {{--<head>--}}
        {{--<meta charset="utf-8">--}}

        {{--@yield('seotags')--}}

        {{--<link href="{{ asset('/vendor/styles/bootstrap-grid.css') }}" rel="stylesheet">--}}
        {{--<link href="{{ asset('/vendor/styles/reset.css') }}" rel="stylesheet">--}}
        {{--<link href="{{ asset('/vendor/styles/fotorama.css') }}" rel="stylesheet">--}}
        {{--<link href="{{ asset('/vendor/styles/layout.css') }}" rel="stylesheet">--}}

        {{--<link href="{{ asset('/assets/styles/site/main.css') }}" rel="stylesheet">--}}
        {{--<link href="{{ asset('/assets/styles/site/wysiwyg.css') }}" rel="stylesheet">--}}

        {{--<script src="{{ asset('/vendor/scripts/jquery.js') }}"></script>--}}
        {{--<script src="{{ asset('/vendor/scripts/fotorama.js') }}"></script>--}}
        {{--<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>--}}
        {{--<script src="{{ asset('/assets/scripts/site/site.js') }}"></script>--}}

        {{--@yield('styles')--}}

        {{--<script src="{{ asset('/assets/scripts/admin/forms.js') }}" type="text/javascript" charset="utf-8" ></script>--}}

        {{--@yield('scripts')--}}
    {{--</head>--}}
    {{--<body>--}}

        {{--<div class="header">--}}
            {{--<div class="tbl-container">--}}
                {{--<div class="col-8p"></div>--}}
                {{--<div class="col-auto header-inner">--}}

                    {{--@include('partials._menu')--}}

                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--<div class="tbl-container ">--}}
            {{--@yield('logo')--}}

            {{--<div class="col-auto">--}}
                {{--@yield('content')--}}
            {{--</div>--}}

        {{--</div>--}}


        {{--@include('partials._tea_list')--}}

    {{--</body>--}}
{{--</html>--}}