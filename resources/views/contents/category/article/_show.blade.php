
<div class="tea-composition col" style="padding-left:20px; width: 240px;">
    {!! Html::picture($content->image_id, ['crop' => true]) !!}
    <br/>
    <br/>

    <div style="padding-right: 40px; font-size: 12px;">
        {!! $content->image_desc !!}
    </div>
</div>

<div class="{!! $content->css_class !!} component-ddowns col">
    {!! $content->desc !!}
</div>

