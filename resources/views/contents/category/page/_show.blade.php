{!! $content->desc !!}

@if ((count($content->slider_ids)))

    <div class="fotorama" data-width="100%" data-nav="thumbs" data-allowfullscreen="true">
        @foreach($content->slider_ids as $image_id)
            {!! Html::picture($image_id, ['size' => '1055x524', 'crop' => true]) !!}
        @endforeach
    </div>

@endif