@extends('layouts.site')

@section('content')

    <tr class="{!! $content->css_class !!} ">

        @include('partials._logo', ['class' => $content->css_class])

        <td class="comps">

            <div class="comps-list">
                @include("contents.comps._{$content->css_class}")

                <div class="instruction-link">
                    <a href="{!! locale_route('contents.instruction', ['id' => $content->seo_id]) !!}">
                        {!! ['ru' => 'инструкция', 'kz' => 'нұсқаулық'][App::getLocale()] !!}
                    </a>
                </div>
            </div>

            <div class="content">
                <h1 class="h h1">
                    {!! $content->title !!}
                </h1>

                <div class="left-col">
                    <span class="sub-h">{!! $content->slogan !!}</span>

                    <ul class="icons">
                        @include("contents.comps.{$content->locale_id}.{$content->css_class}.icons")
                    </ul>

                    <div class="cup"></div>

                </div>
                <div class="right-col"></div>
            </div>


        </td>

    </tr>

@stop