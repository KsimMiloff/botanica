@extends('layouts.site')

@section('content')

    <tr class="{!! $content->css_class !!}">

        @include('partials._logo', ['class' => $content->css_class])

        <td class="content text">
            <h1 class="h h1">
                {!! $content->title !!}
            </h1>

            <div class="left-col">
                <span class="sub-h">{!! $content->slogan !!}</span>

                <div class="sostav wysiwyg">
                    {!! $content->components !!}
                </div>

            </div>

            <div class="right-col">

                <div class="tea-desc wysiwyg">
                    {!! $content->desc !!}
                </div>

                <div class="instruction-link">
                    <a href="{!! locale_route('contents.comps', ['id' => $content->seo_id]) !!}">
                        {!! ['ru' => 'состав', 'kz' => 'құрамы'][App::getLocale()] !!}
                    </a>
                </div>
            </div>

        </td>

    </tr>
@stop