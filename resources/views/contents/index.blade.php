@extends('layouts.site')

@section('content')
    <tr>
        @include('partials._logo')

        <td class="content light text wysiwyg">

            @foreach($contents as $content)

                <?php $content = $content->to_locale($locale); ?>

                @if ($content)
                    <div class="about-section">
                        <div class="about-pic">
                            {!! Html::picture($content->image_id, ['crop' => true]) !!}


                        </div>
                        <div class="about-pic-text">
                            {!! $content->image_desc !!}
                        </div>

                        <div class="about-text">
                            {!! $content->desc !!}
                        </div>

                    </div>
                @endif

            @endforeach

        </td>

    </tr>

@stop
