@extends('layouts.site')

@section('content')

    <tr class="{!! $content->css_class !!}">

        @include('partials._logo')

        <td class="picture-content">
            <a href="{!! locale_route('contents.comps', ['id' => $content->seo_id]) !!}">
                {!! Html::picture($content->page_image_id, ['size' => '1055x524', 'crop' => true, 'style' => 'width: 100%']) !!}
            </a>
        </td>

    </tr>


@stop
