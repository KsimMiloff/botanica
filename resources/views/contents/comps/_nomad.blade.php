<?php
    $comps_desc = [
            ['class' => 'right tor', 'title' => trans('comps.nomad.c1.title'), 'desc' => trans('comps.nomad.c1.desc') ],
            ['class' => 'down tor', 'title' => trans('comps.nomad.c2.title'), 'desc' => trans('comps.nomad.c2.desc') ],
            ['class' => 'down tol', 'title' => trans('comps.nomad.c3.title'), 'desc' => trans('comps.nomad.c3.desc') ],
            ['class' => 'down tor', 'title' => trans('comps.nomad.c4.title'), 'desc' => trans('comps.nomad.c4.desc') ],
            ['class' => 'right tor', 'title' => trans('comps.nomad.c5.title'), 'desc' => trans('comps.nomad.c5.desc') ],
            ['class' => 'down tor', 'title' => trans('comps.nomad.c6.title'), 'desc' => trans('comps.nomad.c6.desc') ],
            ['class' => 'down tor', 'title' => trans('comps.nomad.c7.title'), 'desc' => trans('comps.nomad.c7.desc') ],
            ['class' => 'right tor', 'title' => trans('comps.nomad.c8.title'), 'desc' => trans('comps.nomad.c8.desc') ],
            ['class' => 'down tor', 'title' => trans('comps.nomad.c9.title'), 'desc' => trans('comps.nomad.c9.desc') ],
            ['class' => 'down tor', 'title' => trans('comps.nomad.c10.title'), 'desc' => trans('comps.nomad.c10.desc') ],
            ['class' => 'down tor', 'title' => trans('comps.nomad.c11.title'), 'desc' => trans('comps.nomad.c11.desc') ],
    ];

?>

@foreach ($comps_desc as $k => $value)
    <?php $i = $k + 1 ?>
    <div class="comp ncomp{!! $i !!}">
        <img src="{{ URL::asset("assets/images/nomad/c{$i}.png") }}">
        <span class="comps-title {!! $value['class'] !!}">
            {!! $value['title'] !!}

            <span class="comps-desc">
                {!! $value['desc'] !!}
            </span>
        </span>

    </div>

@endforeach
