<?php
$comps_desc = [
        ['class' => 'right tor', 'title' => trans('comps.yoga.c1.title'), 'desc' => trans('comps.yoga.c1.desc') ],
        ['class' => 'down tor', 'title' => trans('comps.yoga.c2.title'), 'desc' => trans('comps.yoga.c2.desc') ],
        ['class' => 'down tor', 'title' => trans('comps.yoga.c3.title'), 'desc' => trans('comps.yoga.c3.desc') ],
        ['class' => 'down tor', 'title' => trans('comps.yoga.c4.title'), 'desc' => trans('comps.yoga.c4.desc') ],
        ['class' => 'down tor', 'title' => trans('comps.yoga.c5.title'), 'desc' => trans('comps.yoga.c5.desc') ],
        ['class' => 'down tor', 'title' => trans('comps.yoga.c6.title'), 'desc' => trans('comps.yoga.c6.desc') ],
        ['class' => 'down tol', 'title' => trans('comps.yoga.c7.title'), 'desc' => trans('comps.yoga.c7.desc') ],
        ['class' => 'up tor', 'title' => trans('comps.yoga.c8.title'), 'desc' => trans('comps.yoga.c8.desc') ],
        ['class' => 'down tor', 'title' => trans('comps.yoga.c9.title'), 'desc' => trans('comps.yoga.c9.desc') ],
        ['class' => 'down tor', 'title' => trans('comps.yoga.c10.title'), 'desc' => trans('comps.yoga.c10.desc') ],
];

?>

@foreach ($comps_desc as $k => $value)
    <?php $i = $k + 1 ?>
    <div class="comp ycomp{!! $i !!}">
        <img src="{{ URL::asset("assets/images/yoga/c{$i}.png") }}">
        <span class="comps-title {!! $value['class'] !!}">
            {!! $value['title'] !!}

            <span class="comps-desc">
                {!! $value['desc'] !!}
            </span>
        </span>

    </div>

@endforeach
