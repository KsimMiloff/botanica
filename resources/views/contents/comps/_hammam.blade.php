<?php
    $comps_desc = [
        ['class' => 'right tor', 'title' => trans('comps.hammam.c1.title'), 'desc' => trans('comps.hammam.c1.desc') ],
        ['class' => 'down tor', 'title' => trans('comps.hammam.c2.title'), 'desc' => trans('comps.hammam.c2.desc') ],
        ['class' => 'down tor', 'title' => trans('comps.hammam.c3.title'), 'desc' => trans('comps.hammam.c3.desc') ],
        ['class' => 'down tor', 'title' => trans('comps.hammam.c4.title'), 'desc' => trans('comps.hammam.c4.desc') ],
        ['class' => 'right tor', 'title' => trans('comps.hammam.c5.title'), 'desc' => trans('comps.hammam.c5.desc') ],
        ['class' => 'up tol', 'title' => trans('comps.hammam.c6.title'), 'desc' => trans('comps.hammam.c6.desc') ],
        ['class' => 'down tor', 'title' => trans('comps.hammam.c7.title'), 'desc' => trans('comps.hammam.c7.desc') ],
        ['class' => 'left tol', 'title' => trans('comps.hammam.c8.title'), 'desc' => trans('comps.hammam.c8.desc') ],
        ['class' => 'down tor', 'title' => trans('comps.hammam.c9.title'), 'desc' => trans('comps.hammam.c9.desc') ],
    ];

?>

@foreach ($comps_desc as $k => $value)
    <?php $i = $k + 1 ?>
    <div class="comp hcomp{!! $i !!}">
        <img src="{{ URL::asset("assets/images/hammam/c{$i}.png") }}">
        <span class="comps-title {!! $value['class'] !!}">
            {!! $value['title'] !!}

            <span class="comps-desc">
                {!! $value['desc'] !!}
            </span>
        </span>

    </div>

@endforeach
