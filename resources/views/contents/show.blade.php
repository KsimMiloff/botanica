@extends('layouts.site')

@section('content')

    <tr>
        @include('partials._logo')

        <td class="content light text wysiwyg">

            @if(View::exists("contents.category.{$content->category_id}._show"))
                @include("contents.category.{$content->category_id}._show", ['content' => $content])
            @endif

        </td>
    </tr>
@stop
