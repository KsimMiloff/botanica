<?php

$menu = [

    'ContentsController1'  => [
        'title' => 'Контент',

        'subs' => [
            '1' => [
                    'title' => 'Главная',
                    'url'   => route('admin.contents.edit', ['id' => 1]),
            ],
            '2' => [
                    'title' => 'О компании (Виктория)',
                    'url'   => route('admin.contents.edit', ['id' => 2]),
            ],
            '3' => [
                    'title' => 'О компании (Галина)',
                    'url'   => route('admin.contents.edit', ['id' => 12]),
            ],
            '4' => [
                    'title' => 'О заводе',
                    'url'   => route('admin.contents.edit', ['id' => 3]),
            ],
        ]
    ],

    'ContentsController2'  => [
        'title' => 'Чаи',

        'subs' => [
            '4' => [
                    'title' => 'Nomad',
                    'url'   => route('admin.contents.edit', ['id' => 4]),
            ],
            '5' => [
                    'title' => 'Hammam',
                    'url'   => route('admin.contents.edit', ['id' => 5]),
            ],
            '6' => [
                    'title' => 'Yoga',
                    'url'   => route('admin.contents.edit', ['id' => 6]),
            ],
        ]
    ],


    'ContentsController3'  => [
        'title' => 'Точки на карте',
        'url' => route('admin.mpoints.index'),
    ],


    /*'dropdown'  => [
        'title' => 'Настройки',
        'subs' => [
            'ContentsController' => [
                'title' => 'Калькулятор',
                'url'   => route('admin.contents.index'),
            ],
            'ContentsController' => [
                'title' => 'Меню',
                'url'   => route('admin.contents.index'),
            ],
        ]
    ],*/
];

foreach ($menu as $item => $options)
{
    if ( isset( $options['subs'] ) )
    {
        $active[$item] = $item;

        foreach ($options['subs'] as $subitem => $options)
        {
            if ( $subitem == $controller )
            {
                $active[$subitem] = 'active';
                $active[$item] = 'active';
            } else {
                $active[$subitem] = '';
            }
        }

    } else {
        $active[$item] = $item == $controller ? 'active' : '';
    }
}

$active[$controller] = isset($active[$controller]) ? $active[$controller] : '';

//var_dump($active);

?>

<ul class="nav navbar-nav">


    @if (Auth::check())

        @foreach ($menu as $item => $options)

            <li class="{{ $active[$item] }}">

                @if ( isset( $options['subs'] ) )

                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        {{ $options['title'] }}  <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu">
                        @foreach ($options['subs'] as $subitem => $options)

                            <li class="{{ $active[$subitem] }}">
                                <a href="{{ $options['url'] }}">
                                    {{ $options['title'] }}
                                </a>
                            </li>

                        @endforeach
                    </ul>

                @else
                    <a href={{ $options['url'] }}>
                        {{ $options['title'] }}

                        @if (isset($options['badge']) && $options['badge'])
                            <span class="badge">{!! $options['badge'] !!}</span>
                        @endif
                    </a>
                @endif
            </li>

        @endforeach
    @endif
</ul>

<ul class="nav navbar-nav navbar-right">
    @if (Auth::guest())
        <li>{!! link_to_route('auth.login', 'Войти') !!}</li>
    @else
        <li>{!! link_to_route('admin.users.index', 'Пользователи') !!}</li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
               aria-expanded="false">{{ Auth::user()->name }}
                <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                <li>{!! link_to_route('auth.logout', 'Выйти') !!}</li>
            </ul>
        </li>
    @endif
</ul>