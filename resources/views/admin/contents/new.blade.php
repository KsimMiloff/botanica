@extends('layouts.admin')

@section('content')

    @can('everything')
        @include('admin.partials.breadcrumbs', ['crumbs' => $crumbs])
    @endcan

    <div class="page-header">
        <h1>{!! $crumbs->last()['title'] !!} - {!! $content->category->plural_title !!}</h1>
    </div>

    @include('admin.contents.form')
@stop
