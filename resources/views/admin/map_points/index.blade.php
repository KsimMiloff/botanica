@extends('layouts.admin')

@section('content')

    <div class="row">

        <h1>
            Точки на карте
            {!! link_to_route('admin.mpoints.create', 'Добавить', [], ['class' => 'btn btn-primary']) !!}
        </h1>

        <ul class="nav nav-pills">
            <li class="active"><a data-toggle="tab" href="#map">Карта</a></li>
            <li class=""><a data-toggle="tab" href="#points">Точки</a></li>
        </ul>

        <div class="tab-content">

            <div id="map" class="tab-pane fade in active">
                <div id="preview-map" style="height:600px" ></div>
            </div>


            <div id="points" class="tab-pane top-buffer">

                @if ($mpoints->isEmpty())
                    <p>Нет ни одной точки</p>

                @else
                    <table class="table table-hover">
                        <colgroup>
                            <col width="70%"/>
                        </colgroup>
                        <thead>
                        <tr>
                            <th>Заголовок</th>
                            {{--<th>Тип</th>--}}
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($mpoints as $mpoint)
                            <tr>

                                <td> {!! link_to_route('admin.mpoints.edit', $mpoint->title, [$mpoint->id]) !!} </td>

                            </tr>

                        @endforeach

                        </tbody>
                    </table>

                @endif

            </div>
        </div>
    </div>
@stop