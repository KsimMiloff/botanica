<div class="row">

    {!! Form::resource($mpoint, ['route' => 'admin.mpoints', 'class' => 'form-horizontal']) !!}

        <div class="tab-content">

            @include('admin.partials.errors', ['errors' => $mpoint->errors])

        </div>


        <div class="row top-buffer">

            <div class="form-group">
                {!! Form::label('mpoint[title]', 'Заголовок', ['class' => 'control-label col-sm-2'])  !!}
                <div class="col-sm-8">
                    {!! Form::text('mpoint[title]', $mpoint->title, array('class' => 'form-control')) !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('mpoint[address]', 'Адрес', ['class' => 'control-label col-sm-2'])  !!}
                <div class="col-sm-8">
                    {!! Form::textarea('mpoint[address]', $mpoint->address, array('class' => 'form-control', 'rows' => 2)) !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('mpoint[desc]', 'Описание', ['class' => 'control-label col-sm-2'])  !!}
                <div class="col-sm-8">
                    {!! Form::textarea('mpoint[desc]', $mpoint->desc, array('class' => 'form-control', 'rows' => 3)) !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('point', 'Точка на карте', ['class' => 'control-label col-sm-2'])  !!}
                <div class="col-sm-8">
                    {!! Form::hidden('mpoint[lat]', $mpoint->lat) !!}
                    {!! Form::hidden('mpoint[lon]', $mpoint->lon) !!}
                    <div id="touchable-map" style="height:400px"></div>
                </div>
            </div>
        </div>


        @include("admin.partials.form._button_bar", ['object' => $mpoint, 'lifecycled' => true])


    {!! Form::close() !!}

</div>
