@extends('layouts.admin')

@section('content')

    @can('everything')
        @include('admin.partials.breadcrumbs', ['crumbs' => $crumbs])
    @endcan

    <div class="page-header">
        <h1>{!! $crumbs->last()['title'] !!}</h1>
    </div>

    @include('admin.map_points.form')
@stop
