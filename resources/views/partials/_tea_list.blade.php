<br>
<br>
<div class="container">
    <div class="row">
        <div class="col-xs-offset-2">


            @foreach($tea_list as $tea_item)
                <?php $localed_item = $tea_item->to_locale($locale); ?>

                @if ($localed_item)

                    <div class="col-md-4">
                        <div class="tea-menu-i">

                            <a class="tea-menu-i-image" href="{!! locale_route('contents.picture', ['id' => $localed_item->seo_id]) !!}">
                                {!! Html::picture($localed_item->image_id, ['size' => '133x111', 'crop' => true, 'class' => 'face']) !!}
                            </a>

                            <div class="tea-menu-link">
                                <a href="{!! locale_route('contents.picture', ['id' => $localed_item->seo_id]) !!}">
                                    <img src="{!! URL::asset('assets/images/tea-menu-arrow.png') !!}" class="arrow2">

                                    <span>
                                        {!! $localed_item->title !!}
                                        <br/>
                                        <small>{!! $localed_item->slogan !!}</small>
                                    </span>
                                </a>
                            </div>
                        </div>

                        <br/>
                    </div>

                @endif

            @endforeach

        </div>
    </div>
</div>