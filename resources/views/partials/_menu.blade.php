
<ul class="menu">
    @foreach($site_menu as $item)
        <li class="{!! (isset($menu_id) && in_array($menu_id , $item['id'])? 'active' : '') !!}">
            @if (isset($item['url']))
                <a href="{!! $item['url'] !!}">{!! $item['title'] !!}</a>
            @elseif((isset($item['subs']) && count($item['subs']) ))
                {!! $item['title'] !!} <span class="arrow-down"></span>
                <ul>
                    @foreach($item['subs'] as $sitem)
                        <li><a href="{!! $sitem['url'] !!}">{!! $sitem['title'] !!}</a></li>
                    @endforeach
                </ul>
            @endif
        </li>

    @endforeach
</ul>

<ul class="menu lang-menu">

    <?php $langs = ['ru' => 'рус', 'kz' => 'каз']; ?>
    <li class="lang-menu">
        {!! $langs[App::getLocale()] !!} <span class="arrow-down"></span>
        <ul>
            @foreach($langs as $lang => $title)
                <li><a href="/{!! $lang !!}">{!! $title !!}</a></li>
            @endforeach
        </ul>
    </li>

</ul>